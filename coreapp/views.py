#views.py

from coreapp import app
from flask import request
import datetime


@app.route('/')

def index():
	
	page = '''
	
		<DOCTYPE! html>
		
		<html lang = 'en-US'>
		
			<head>
				<meta charset = 'utf-8'>
				<title>Weekday Generator</title>
			</head>
			
			<body>
				<h1 >Enter a Date</h1>
				
				<div>
					Please enter the month, day, and year for the weekday you'd like to find.
				</div>
				
				<br>
				
				<div>		
 				
 				<form action='/dow' method='GET'>
 				
 				<select name = 'day'>
 							<option value = '1'> 1 </option>
 							<option value = '2'> 2 </option>
 							<option value = '3'> 3 </option>
 							<option value = '4'> 4 </option>
 							<option value = '5'> 5 </option>
 							<option value = '6'> 6 </option>
 							<option value = '7'> 7 </option>
 							<option value = '8'> 8 </option>
 							<option value = '9'> 9 </option>
 							<option value = '10'> 10 </option>
 							<option value = '11'> 11 </option>
 							<option value = '12'> 12 </option>
 							<option value = '13'> 13 </option>
 							<option value = '14'> 14 </option>
 							<option value = '15'> 15 </option>
 							<option value = '16'> 16 </option>
 							<option value = '17'> 17 </option>
 							<option value = '18'> 18 </option>
 							<option value = '19'> 19 </option>
 							<option value = '20'> 20 </option>
 							<option value = '21'> 21 </option>
 							<option value = '22'> 22 </option>
 							<option value = '23'> 23 </option>
 							<option value = '24'> 24 </option>
 							<option value = '25'> 25 </option>
 							<option value = '26'> 26 </option>
 							<option value = '27'> 27 </option>
 							<option value = '28'> 28 </option>
 							<option value = '29'> 29 </option>
 							<option value = '30'> 30 </option>
 							<option value = '31'> 31 </option>
 						</select>
 				
 				
 				<select  name = 'month' >
 							<option value = '1'> 1 </option>
 							<option value = '2'> 2 </option>
 							<option value = '3'> 3 </option>
 							<option value = '4'> 4 </option>
 							<option value = '5'> 5 </option>
 							<option value = '6'> 6 </option>
 							<option value = '7'> 7 </option>							
 							<option value = '8'> 8 </option>
 							<option value = '9'> 9 </option>
 							<option value = '10'> 10 </option>
 							<option value = '11'> 11 </option>
 							<option value = '12'> 12 </option>
 						</select>
 			
 				
 			
 				<select name = 'year'>
 							<option value = '2014'> 2014 </option>
 							<option value = '2015'> 2015 </option>
 							<option value = '2016'> 2016 </option>
 							<option value = '2017'> 2017 </option>
 							<option value = '2018'> 2018 </option>
 							<option value = '2019'> 2019 </option>
 							<option value = '2020'> 2020 </option>
 						</select>
 			
 						<input type = 'submit' value='Submit'>	
 			
 			</form>
 			
 			</div>
			</body>
		
		</html>
	'''
	return page
	
@app.route('/dow', methods=['get'])
 
def week():
 	
 	try:
 	
 		month	=	int(request.args.get('month',''))
 		day		=	int(request.args.get('day',''))
 		year	=	int(request.args.get('year',''))
 	
 		date_search = datetime.date(year,month,day).weekday()

 		page2 = '''
 		
 			<DOCTYPE! html>
 		
 			<html lang = "en-US">
 		
 				<head>
 		
 					<meta charset = "utf-8">
 					<title>Day of the Week</title>
 			
 				</head>
 		
 				<body>
 				
 					<div>
 						<h1>Day of the Week:</h1>
 						<br>
 					</div>
 					
 				</body>
 			</html>
 		'''
 		return page2 + '<center>' + str(choose_week_day(date_search)) + '</center>'
 	
 	except Exception as e:
 		error_msg = str(e)
 	
def choose_week_day(weekday):

	if weekday == 0:
		
		return 'Monday'
		
	elif weekday == 1:
		
		return 'Tuesday'
		
	elif weekday == 2:
	
		return 'Wednesday'
		
	elif weekday == 3:
	
		return 'Thursday'
		
	elif weekday == 4:
	
		return 'Friday'
		
	elif weekday == 5:
	
		return 'Saturday'
		
	elif weekday == 6:
	
		return 'Sunday'
